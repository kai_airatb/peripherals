.model tiny

; task:	| text | row | col | foreground | background |
;	|------|-----|-----|------------|------------|
;	| BA   | 5   | 20  | red        | white      |

.code
	mov ah, 0h		; Set function to configure video mode
	mov al, 3h		; Set configuration to text mode 80x25
	int 10h			; Call function to configure it out

	mov ax, 0b800h		; The video address segment
	mov ds, ax

	mov di, 2*(5*80+20)	; Character position (index) = 2 * (row * 80 + col)
	mov bh, 01110100b	; Configure attributes: white background, red foreground

	mov [ds]:[di], 'B'	; Set character
	inc di
	mov [ds]:[di], bh	; Set character attributes
	inc di
	mov [ds]:[di], 'A'	; Set second character
	inc di
	mov [ds]:[di], bh	; Set character attributes

	mov ah, 0h		; Function to read key press
	int 16h			; Wait for key press

	mov ah, 4ch		; Function to exit back to DOS
	int 21h			; Exit to DOS
end
