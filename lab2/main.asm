.model tiny

; task:	| X1 | X2  | Y1 | Y2 | Color     |
;	|----|-----|----|----|-----------|
;	| 30 | 110 | 20 | 60 | light-red |

.code
	mov ah, 0h		; Set function to configure video mode
	mov al, 13h		; Set configuration to ghraphical 320x200
	int 10h			; Call function to configure it out

	mov ax, 0b800h		; The video address segment
	mov ds, ax

	mov di, 2*(5*80+20)	; Character position (index) = 2 * (row * 80 + col)
	mov bh, 01110100b	; Configure attributes: white background, red foreground

	mov cx, 30		; set X1
	mov dx, 20		; set Y1
	mov al, 128		; set Color
	call draw

row:
	mov ah, 0ch		; function to change pixel color
	mov al, 128		; set color
				; other registers already set
	int 10h			; change color
	inc cx			; increment X position
	cmp cx, 110		; do it until X = 110
	jne row

draw:
	mov cx, 30		; set X1
	inc dx
	cmp dx, 60
	jne row
	call exit

exit:
	mov ah, 0h		; Function to read key press
	int 16h			; Wait for key press

	mov ah, 0h		; Set function to configure video mode
	mov al, 3h		; Set configuration to ghraphical 320x200
	int 10h			; Call function to configure it out

	mov ah, 4ch		; Function to exit back to DOS
	int 21h			; Exit to DOS
end
